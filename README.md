# Oracle Database Install Guides

<p align="center">

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/donate/?hosted_button_id=W45Y5ENDC7M9C)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/jeffreyhunter/oracle-database-install-guides/-/blob/main/LICENSE)
[![Maintainer](https://img.shields.io/badge/maintainer-Jeffrey%20M.%20Hunter-blue)](https://gitlab.com/jeffreyhunter)
[![LinkedIn Profile](https://img.shields.io/badge/LinkedIn-oraclejavanet-blue?style=for-the-badge&logo=linkedin)](https://www.linkedin.com/in/oraclejavanet/)

</p>

## Overview

<p>
    <b>Coming in Fall 2024</b>
</p>

A collection of install guides written by
[Jeffrey M. Hunter](https://gitlab.com/jhunter) that offer step-by-step
instructions for installing Oracle Database on the Unix/Linux platform.

## Linux

* [Install Oracle Database 21c (RHEL 8)](doc/install-oracle-database-21c-rhel-8.md)
* [Install Oracle Database 19c (RHEL 8)](doc/install-oracle-database-19c-rhel-8.md)
* [Install Oracle Database 19c (RHEL 7)](doc/install-oracle-database-19c-rhel-7.md)
* [Install Oracle Database 18c (RHEL 7)](doc/install-oracle-database-18c-rhel-7.md)
* [Install Oracle Database 12c (RHEL 7)](doc/install-oracle-database-12c-rhel-7.md)
* [Install Oracle Database 11g (RHEL 7)](doc/install-oracle-database-11g-rhel-7.md)

## Oracle Solaris

* [Install Oracle Database 19c (Solaris 11.4)](doc/install-oracle-database-19c-solaris-11.4.md)

## Supplemental Guides

* [Oracle LVM Configuration](doc/oracle-lvm-configuration.md)
* [Extend Volume Group](doc/extend-volume-group.md)
* [Extend Logical Volume](doc/extend-volume-group.md)
* [Extend root File System](doc/extend-root-file-system.md)
* [Rename a Volume Group](doc/rename-a-volume-group.md)
* [Change Default Kernel on Oracle Linux](doc/change-default-kernel-on-oracle-linux.md)

## License

Copyright (c) 2024 Jeffrey M. Hunter.

GitLab: [Jeffrey M. Hunter](https://gitlab.com/jhunter)

This project is licensed under the
[MIT License](https://gitlab.com/jeffreyhunter/oracle-database-install-guides/-/blob/main/LICENSE).
Feel free to use, modify, and distribute the code in accordance with the terms
of the license.

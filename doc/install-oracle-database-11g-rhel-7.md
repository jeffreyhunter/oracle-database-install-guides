# Install Oracle Database 11g (RHEL 7)

## Table of Contents



## Synopsis

This document is a comprehensive guide for installing Oracle Database 11g
Release 2 (11.2.0.4) on the Oracle Linux 7 operating system. This guide will
also work for Red Hat Enterprise Linux 7 and other RHEL derivatives such as
CentOS 7 with only minor modifications.

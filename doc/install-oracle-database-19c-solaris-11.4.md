# Install Oracle Database 19c (Solaris 11.4)

[//]: # (> **&#9888;** &nbsp; **Warning**)
[//]: # (> **&#9432;** &nbsp; **Information**)
[//]: # (> **&#128465;** &nbsp; **Trash can**)
[//]: # (> **&#128274;** &nbsp; **Lock**)
[//]: # (> **&#128161;** &nbsp; **Light bulb**)

## Table of Contents

* [Synopsis](#synopsis)
* [Install Oracle Solaris Operating System](#install-oracle-solaris-operating-system)
* [Prepare Operating System Environment](#prepare-operating-system-environment)
* [Prepare Storage for Oracle](#prepare-storage-for-oracle)
* [Create OS Users](#create-os-users)
* [Hardware and Operating System Requirements](#hardware-and-operating-system-requirements)
* [Install Solaris Desktop (for VNC Server)](#install-solaris-desktop-for-vnc-server)
* [Download Oracle Database Software](#download-oracle-database-software)
* [Install Oracle Database Software](#install-oracle-database-software)
* [Install the Oracle Examples](#install-the-oracle-examples)
* [Configure Oracle Networking](#configure-oracle-networking)
* [Install Autonomous Health Framework (AHF)](#install-autonomous-health-framework-ahf)
* [Apply Latest Database Patchset](#apply-latest-database-patchset)
* [Create Oracle Database](#create-oracle-database)
* [Connect to Enterprise Manager (EM) Express](#connect-to-enterprise-manager-em-express)
* [Configure Database Automatic Startup and Shutdown](#configure-database-automatic-startup-and-shutdown)
* [Managing Oracle Services in Solaris](#managing-oracle-services-in-solaris)
* [References](#references)

## Synopsis

This document is a comprehensive guide for installing Oracle Database 19c (19.3)
on the Oracle Solaris 11.4 x86 operating system. This guide will also work for
Oracle Solaris SPARC with only minor modifications.

The example database created in this guide will use the new
_Multitenant Architecture_ introduced with Oracle Database 12c to create a
container database (CDB) and a pluggable database (PDB).

> **&#9888;** &nbsp; **CDB-Only Architecture**
>
> The non-CDB architecture was deprecated in Oracle Database 12c and will be
> officially de-supported in Oracle Database 21c.
>
> Oracle Database 21c will be the first Oracle database release with a CDB-only
> architecture which means that the Oracle Universal Installer and DBCA can no
> longer be used to create non-CDB Oracle Database instances.

All physical database files (data files, control files, online redo log files,
fast recovery area) will be created on local disks using the Oracle Solaris ZFS
file system. Archive log mode and flashback database will be configured
to support database recovery. Lastly, procedures will be put into effect to
automatically start/stop the database on system boot.

In addition to the Oracle database, the Oracle Net Listener and Oracle
Enterprise Manager Database Express (EM Express) will be configured to run on
the local system.

This guide does not cover the steps to install the Oracle database software on a
system that already has an existing Oracle software installation. The
installation described in this guide only includes instructions for installing
the Oracle database software on a system with no previous Oracle installation.

**Oracle Documentation**

This guide provides detailed instructions for successfully installing and
configuring Oracle Database; however, it is by no means a substitute for the
official Oracle documentation.

In addition to this guide, users should also consult the following Oracle
documentation to gain a full understanding of alternative configuration options,
installation, and administration with Oracle Database 19c.

* Oracle's official documentation site:\
  [https://docs.oracle.com/](https://docs.oracle.com/)
  
* Oracle Database Installation Guide 19c for Oracle Solaris:\
  [https://docs.oracle.com/en/database/oracle/oracle-database/19/ssdbi/index.html](https://docs.oracle.com/en/database/oracle/oracle-database/19/ssdbi/index.html)
  
* Grid Infrastructure Installation and Upgrade Guide 19c for Oracle Solaris:\
  [https://docs.oracle.com/en/database/oracle/oracle-database/19/cwsol/index.html](https://docs.oracle.com/en/database/oracle/oracle-database/19/cwsol/index.html)

**Oracle Database Prerequisites Packages for Oracle Solaris**

This guide does not make use of the Oracle Database prerequisites group package
for Oracle Solaris:

`group/prerequisite/oracle/oracle-database-preinstall-19c`

For example:

```
# pkg install oracle-database-preinstall-19c
```

All Oracle Database pre-installation requirements will be manually performed.

One significant reason for preferring manual execution of all pre-installation
requirements is the concern for security. Many system administrators may have
security concerns with installing bundled packages. They might prefer to
manually install each dependency to have better visibility and control over the
installation process.

For further information, see
[Completing Preinstallation Tasks Manually](https://docs.oracle.com/en/database/oracle/oracle-database/19/ssdbi/completing-preinstallation-tasks-manually.html)
in the
[Database Installation Guide 19c for Oracle Solaris](https://docs.oracle.com/en/database/oracle/oracle-database/19/ssdbi/index.html).

**Optimal Flexible Architecture (OFA)**

This guide adheres to the latest
[Optimal Flexible Architecture](https://docs.oracle.com/en/database/oracle/oracle-database/19/ssdbi/optimal-flexible-architecture.html)
standard for Oracle Database 19c (19.3) for Oracle Solaris.

**Databases**



**Disks**



**ZFS Storage Configuration**



**ZFS File Systems and Mount Points**

| ZFS Pool Name  | File System              | Mount Point  | Purpose                             |
| -------------- | ------------------------ | ------------ | ----------------------------------- |
| rpool          | rpool/ROOT/11.4.42.111.0 | /            | Oracle Solaris 11 Operating System  |
| dbpool         | dbpool/u01               | /u01         | Oracle Home                         |
|                | dbpool/u02               | /u02         | Oracle Database Files               |
|                | dbpool/u03               | /u03         | Oracle Fast Recovery Area           |
|                | dbpool/u04               | /u04         | Oracle Data Pump                    |

**Oracle Home File Paths**

| Directory          | File Path                                             |
| ------------------ | ----------------------------------------------------- |
| ORACLE_BASE        | /u01/app/oracle                                       |
| ORACLE_HOME        | /u01/app/oracle/product/19.0.0/dbhome_1               |
| ORACLE_BASE_HOME   | /u01/app/oracle/product/19.0.0/dbhome_1               |
| ORACLE_BASE_CONFIG | /u01/app/oracle/product/19.0.0/dbhome_1               |
| TNS_ADMIN          | /u01/app/oracle/product/19.0.0/dbhome_1/network/admin |
| dbs                | /u01/app/oracle/product/19.0.0/dbhome_1/dbs           |
| hs                 | /u01/app/oracle/product/19.0.0/dbhome_1/hs/admin      |
| mgw                | /u01/app/oracle/product/19.0.0/dbhome_1/mgw/admin     |
| drdaas             | /u01/app/oracle/product/19.0.0/dbhome_1/drdaas/admin  |
| AHF_HOME           | /opt/oracle.ahf                                       |
| AHF_DATA_DIR       | /opt/oracle.ahf/data                                  |

* To print the `ORACLE_BASE_HOME` path, run:

  ```
  ORACLE_HOME=/u01/app/oracle/product/19.0.0/dbhome_1
  cd $ORACLE_HOME/bin
  ./orabasehome
  ```

  or

  ```
  echo $(orabasehome)
  ```

* To print the `ORACLE_BASE_CONFIG` path, run:

  ```
  ORACLE_HOME=/u01/app/oracle/product/19.0.0/dbhome_1
  cd $ORACLE_HOME/bin
  ./orabaseconfig
  ```

  or

  ```
  echo $(orabaseconfig)
  ```

## Install Oracle Solaris Operating System

_Pending_

## Prepare Operating System Environment

_Pending_

## Prepare Storage for Oracle

_Pending_

## Create OS Users

_Pending_

## Hardware and Operating System Requirements

_Pending_

## Install Solaris Desktop (for VNC Server)

_Pending_

## Download Oracle Database Software

_Pending_

## Install Oracle Database Software

_Pending_

## Install the Oracle Examples

_Pending_

## Configure Oracle Networking

_Pending_

## Install Autonomous Health Framework (AHF)

_Pending_

## Apply Latest Database Patchset

_Pending_

## Create Oracle Database

_Pending_

## Connect to Enterprise Manager (EM) Express

_Pending_

## Configure Database Automatic Startup and Shutdown

_Pending_

## Managing Oracle Services in Solaris

_Pending_

## References

* [Requirements for Installing Oracle Database 19c on Oracle Solaris 11 on SPARC (64-Bit) (Doc ID 2552551.1)](https://support.oracle.com/epmos/faces/DocumentDisplay?_afrLoop=394070365800835&parent=EXTERNAL_SEARCH&sourceId=REFERENCE&id=2552551.1)
* [Creating a Service to Start or Stop an Oracle Database Instance](https://docs.oracle.com/cd/E37838_01/html/E61677/odbstartstop.html)
* [Creating an Oracle Database Listener Service](https://docs.oracle.com/cd/E37838_01/html/E61677/odblistener.html)
* [Viewing Service Log Files](https://docs.oracle.com/cd/E36784_01/html/E36820/viewlog.html)

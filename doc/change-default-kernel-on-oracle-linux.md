# Change Default Kernel on Oracle Linux

## Table of Contents

1. [Synopsis](#synopsis)
2. [Change Default Kernel in Oracle Linux 8](#change-default-kernel-in-oracle-linux-8)
3. [Change Default Kernel in Oracle Linux 7](#change-default-kernel-in-oracle-linux-7)
4. [Change Default Kernel in Oracle Linux 6 and 5](#change-default-kernel-in-oracle-linux-6-and-5)
5. [Display Kernel Examples](#display-kernel-examples)
6. [References](#references)

## Synopsis

Oracle Linux comes with a choice of two kernels, the
[Unbreakable Enterprise Kernel (UEK)](https://www.oracle.com/linux/technologies/),
which is installed and enabled by default, and the Red Hat Compatible Kernel
(RHCK).

The aim of this guide is to illustrate how you change the default kernel (UEK)
and boot into the installed Red Hat compatible kernel.

## Change Default Kernel in Oracle Linux 8

In Oracle Linux 8, use the **grubby** command to manage the system boot
requirements.

The command line will be used in this section to change the default kernel from
(UEK) and boot into the installed Red Hat compatible kernel.

The instance below is running Oracle Linux 8.5 and has the Unbreakable
Enterprise Kernel (UEK) and Red Hat Compatible Kernel (RHCK) installed.

```bash
$ ls -l /boot/vmlinuz*
-rwxr-xr-x. 1 root root 10348096 Apr 12 13:41 /boot/vmlinuz-0-rescue-cb98a6013ecb4ab894f3508a234fb059
-rwxr-xr-x. 1 root root 10212912 Nov 10  2021 /boot/vmlinuz-4.18.0-348.el8.x86_64
-rwxr-xr-x. 1 root root 10348096 Oct  8  2021 /boot/vmlinuz-5.4.17-2136.300.7.el8uek.x86_64
```

Use the **grubby --default-kernel** command to view the current default kernel.
In this case, the UEK is the default running kernel.

```bash
$ sudo grubby --default-kernel
/boot/vmlinuz-5.4.17-2136.300.7.el8uek.x86_64
```

The **grubby --set-default** command can be used to change the default kernel to
the one you specify. Below, the default kernel is being changed to the installed
RHCK.

```bash
$ sudo grubby --set-default /boot/vmlinuz-4.18.0-348.el8.x86_64
The default is /boot/loader/entries/cb98a6013ecb4ab894f3508a234fb059-4.18.0-348.el8.x86_64.conf with index 1 and kernel /boot/vmlinuz-4.18.0-348.el8.x86_64
```

After modifying the GRUB configuration file, reboot the instance.

```bash
$ sudo reboot
```

After the instance is up and running, verify the system is running the RHCK
kernel.

```bash
$ sudo grubby --default-kernel
/boot/vmlinuz-4.18.0-348.el8.x86_64
```

## Change Default Kernel in Oracle Linux 7

## Change Default Kernel in Oracle Linux 6 and 5

## Display Kernel Examples

## References

* [Oracle Linux: How to Change Default Kernel on GRUB2 Using the grubby Tool (Doc ID 2605359.1)](https://support.oracle.com/epmos/faces/DocumentDisplay?_afrLoop=66520347306247&parent=EXTERNAL_SEARCH&sourceId=HOWTO&id=2605359.1)
